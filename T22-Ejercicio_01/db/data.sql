
CREATE DATABASE IF NOT EXISTS `MVC_db01`;
USE `MVC_db01`;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (nombre, apellido, direccion, dni) VALUES 
('Jaume', 'Lopez', 'Lubowitz Valleys', '12048597'),
('Joan', 'Marsal', 'Darion Summit', '31245198'),
('Jordi', 'Rubio', 'Darrel Skyway', '16248579'),
('Arnau', 'Aladid', 'Jacobson Crossroad', '15249963'),
('Daniel', 'Sopena', 'Jewel Spurs', '12345625'),
('Luis', 'Martinez', 'Christiansen Crest', '93846273'),
('Laia', 'fernandez', 'Paris Gateway', '1624852');

