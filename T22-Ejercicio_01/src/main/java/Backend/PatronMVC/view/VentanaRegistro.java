package Backend.PatronMVC.view;

import java.awt.event.*;
import javax.swing.*;
import Backend.PatronMVC.controller.ClienteController;
import Backend.PatronMVC.model.dto.Cliente;


public class VentanaRegistro extends JFrame implements ActionListener{

	
	private static final long serialVersionUID = 1L;
	private ClienteController clienteController; //objeto clienteController que permite la relacion entre esta clase y la clase ClienteController
	private JLabel labelTitulo;
	private JTextField textNombre, textApellido, textdireccion, textDni;
	private JLabel nombre, dni, apellido, direccion;
	private JButton botonGuardar, botonCancelar;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana de registro
	 */
	public VentanaRegistro() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(110, 220, 120, 25);
		botonGuardar.setText("Registrar");
		
		botonCancelar = new JButton();
		botonCancelar.setBounds(250, 220, 120, 25);
		botonCancelar.setText("Cancelar");

		labelTitulo = new JLabel();
		labelTitulo.setText("REGISTRO DE CLIENTES");
		labelTitulo.setBounds(120, 20, 380, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 18));
		
		nombre=new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(20, 120, 80, 25);
		add(nombre);

		dni=new JLabel();
		dni.setText("Dni");
		dni.setBounds(290, 160, 80, 25);
		add(dni);
		
		direccion=new JLabel();
		direccion.setText("Direccion");
		direccion.setBounds(20, 160, 80, 25);
		add(direccion);
		
		apellido=new JLabel();
		apellido.setText("Apellido");
		apellido.setBounds(290, 120, 80, 25);
		add(apellido);

		textNombre=new JTextField();
		textNombre.setBounds(80, 120, 190, 25);
		add(textNombre);

		textDni=new JTextField();
		textDni.setBounds(340, 160, 80, 25);
		add(textDni);
		
		textdireccion=new JTextField();
		textdireccion.setBounds(80, 160, 190, 25);
		add(textdireccion);
		
		textApellido=new JTextField();
		textApellido.setBounds(340, 120, 80, 25);
		add(textApellido);
		
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		add(botonCancelar);
		add(botonGuardar);
		add(labelTitulo);
		limpiar();
		setSize(480, 300);
		setTitle("Patron de Diseño/MVC");
		setLocationRelativeTo(null);
		setResizable(false);
		setLayout(null);

	}


	private void limpiar() 
	{
		textNombre.setText("");
		textApellido.setText("");
		textdireccion.setText("");
		textDni.setText("");
	}


	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}


	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==botonGuardar)
		{
			try {
				Cliente miCliente=new Cliente();
				miCliente.setNombreCliente(textNombre.getText());
				miCliente.setApellidoCliente(textApellido.getText());
				miCliente.setDireccionCliente(textdireccion.getText());
				miCliente.setDniCliente(Integer.parseInt(textDni.getText()));
				
				clienteController.registrarCliente(miCliente);	
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource()==botonCancelar)
		{
			this.dispose();
		}
	}
	
	

}
